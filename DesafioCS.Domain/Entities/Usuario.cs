﻿using System;
using System.Collections.Generic;

namespace DesafioCS.Domain.Entities
{
    public class Usuario
    {
        public Usuario()
        {
            Id = Guid.NewGuid();
            DataCriacao = DateTime.Now;
            DataAtualizacao = DateTime.Now;
            UltimoLogin = DateTime.Now;
        }

        public Guid Id { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string Senha { get; set; }

        public List<Telefone> Telefones { get; set; }

        public DateTime DataCriacao { get; set; }

        public DateTime DataAtualizacao { get; set; }

        public DateTime UltimoLogin { get; set; }

        public string Token { get; set; }

        public bool ValidarSenha(string senha)
        {
            return Senha == senha;
        }

        public bool ValidarToken(string token)
        {
            return Token == token;
        }

        public void AtualizarUltimoLogin()
        {
            UltimoLogin = DateTime.Now;
        }
    }
}
