﻿using System;
using DesafioCS.Domain.Entities;
using DesafioCS.Domain.Interfaces.Repositories;
using DesafioCS.Domain.Interfaces.Services;

namespace DesafioCS.Domain.Services
{
    public class UsuarioService : ServiceBase<Usuario>, IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioService(IUsuarioRepository usuarioRepository)
            : base(usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        public new Usuario Obter(Func<Usuario, bool> expr)
        {
            return _usuarioRepository.Obter(expr);
        }
    }
}
