﻿using DesafioCS.Domain.Interfaces.Repositories;
using DesafioCS.Domain.Interfaces.Services;
using System;
using System.Linq.Expressions;

namespace DesafioCS.Domain.Services
{
    public class ServiceBase<TEntity> : IDisposable, IServiceBase<TEntity> where TEntity : class
    {
        private readonly IRepositoryBase<TEntity> _repository;

        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }

        public void Inserir(TEntity obj)
        {
            _repository.Inserir(obj);
        }

        public bool Any(Expression<Func<TEntity, bool>> expr)
        {
            return _repository.Any(expr);
        }

        public TEntity Obter(Func<TEntity, bool> expr)
        {
            return _repository.Obter(expr);
        }

        public void Atualizar(TEntity obj)
        {
            _repository.Atualizar(obj);
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}
