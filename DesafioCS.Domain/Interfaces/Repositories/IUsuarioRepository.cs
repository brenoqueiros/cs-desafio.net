﻿using DesafioCS.Domain.Entities;
using System;

namespace DesafioCS.Domain.Interfaces.Repositories
{
    public interface IUsuarioRepository : IRepositoryBase<Usuario>
    {
        new Usuario Obter(Func<Usuario, bool> expr);
    }
}
