﻿using DesafioCS.Domain.Entities;
using System;

namespace DesafioCS.Domain.Interfaces.Services
{
    public interface IUsuarioService : IServiceBase<Usuario>
    {
        new Usuario Obter(Func<Usuario, bool> expr);
    }
}