﻿using DesafioCS.Application;
using DesafioCS.Application.Interfaces;
using DesafioCS.Domain.Interfaces.Repositories;
using DesafioCS.Domain.Interfaces.Services;
using DesafioCS.Domain.Services;
using DesafioCS.Infra.Data.Repositories;
using SimpleInjector;

namespace DesafioCS.Infra.CrossCutting.IoC
{
    public class BootStrapper
    {
        public static void RegisterServices(Container container)
        {
            container.RegisterPerWebRequest<IUsuarioAppService, UsuarioAppService>();
            container.RegisterPerWebRequest<IUsuarioService, UsuarioService>();
            container.RegisterPerWebRequest<IUsuarioRepository, UsuarioRepository>();
        }
    }
}
