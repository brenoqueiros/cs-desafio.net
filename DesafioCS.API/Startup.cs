﻿using Microsoft.Owin.Cors;
using Owin;
using SimpleInjector.Integration.WebApi;
using System.Web.Http;
using SimpleInjector;
using DesafioCS.Infra.CrossCutting.IoC;
using DesafioCS.API.CustomAttributes;
using DesafioCS.Application.AutoMapper;
using Newtonsoft.Json;

namespace DesafioCS.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            ConfigureWebApi(config);
            ConfigureDependencyInjection(config);

            AutoMapperConfig.InitializeMappings();

            app.UseCors(CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        private void ConfigureWebApi(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "ResourceNotFound",
                routeTemplate: "api/{*uri}",
                defaults: new { controller = "Default", uri = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Filters.Add(new CustomExceptionFilterAttribute());

            config.Formatters.Remove(config.Formatters.XmlFormatter);

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            config.Formatters.JsonFormatter.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            config.Formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Unspecified;
        }

        public void ConfigureDependencyInjection(HttpConfiguration config)
        {
            using (var container = new Container())
            {
                container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();

                BootStrapper.RegisterServices(container);

                container.RegisterWebApiControllers(config);

                container.Verify();

                config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
            }
        }
    }
}