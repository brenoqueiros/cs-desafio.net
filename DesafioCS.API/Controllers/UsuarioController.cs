﻿using DesafioCS.API.CustomAttributes;
using DesafioCS.Application.DTO;
using DesafioCS.Application.Interfaces;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DesafioCS.API.Controllers
{
    [RoutePrefix("api")]
    public class UsuarioController : ApiController
    {
        private readonly IUsuarioAppService _usuarioAppService;

        public UsuarioController(IUsuarioAppService usuarioAppService)
        {
            _usuarioAppService = usuarioAppService;
        }

        [Route("SignUp")]
        public HttpResponseMessage PostSignUp(UsuarioDTO usuario)
        {
            var usuarioRegistrado = _usuarioAppService.Registrar(usuario);

            return Request.CreateResponse(HttpStatusCode.OK, usuarioRegistrado);
        }

        [Route("Login")]
        public HttpResponseMessage PostLogin(UsuarioDTO usuario)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _usuarioAppService.EfetuarLogin(usuario));
        }

        [CustomAuthorize]
        [Route("Profile/{id:Guid}")]
        public HttpResponseMessage GetProfile(Guid id)
        {
            var bearer = Request.Headers.Authorization;

            return Request.CreateResponse(HttpStatusCode.OK, _usuarioAppService.ObterPerfil(id, bearer.Parameter));
        }
    }
}