﻿using DesafioCS.Infra.CrossCutting.CustomException.Exceptions;
using System.Web.Http;

namespace DesafioCS.API.Controllers
{
    public class DefaultController : ApiController
    {
        public IHttpActionResult Get(string uri)
        {
            throw new ResourceNotFoundException();
        }
    }
}
