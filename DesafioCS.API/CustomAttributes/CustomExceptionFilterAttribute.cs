﻿using System.Net.Http;
using System.Web.Http.Filters;
using System.Net;
using DesafioCS.Infra.CrossCutting.CustomException;

namespace DesafioCS.API.CustomAttributes
{
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private const HttpStatusCode DEFAULT_STATUS_CODE = HttpStatusCode.InternalServerError;

        private const string DEFAULT_ERROR_MESSAGE = "Ocorreu um erro interno de servidor";

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is ICustomException)
            {
                var customException = (ICustomException)actionExecutedContext.Exception;

                SetCustomResponseError(customException.Codigo, customException.Mensagem, actionExecutedContext);
            }
            else
            {
                SetCustomResponseError(DEFAULT_STATUS_CODE, DEFAULT_ERROR_MESSAGE, actionExecutedContext);
            }
        }

        private void SetCustomResponseError(HttpStatusCode statusCode, string mensagem, HttpActionExecutedContext actionExecutedContext)
        {
            actionExecutedContext.Response = actionExecutedContext
                                                .Request
                                                .CreateResponse(statusCode,
                                                new
                                                {
                                                    statusCode = (int)statusCode,
                                                    mensagem = mensagem
                                                });
        }
    }
}
