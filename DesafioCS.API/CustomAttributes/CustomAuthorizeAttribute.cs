﻿using DesafioCS.Infra.CrossCutting.CustomException.Exceptions;
using System;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace DesafioCS.API.CustomAttributes
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var auth = actionContext.Request.Headers.Authorization;

            if (auth == null || string.IsNullOrEmpty(auth.Scheme) || auth.Scheme != "Bearer" || string.IsNullOrEmpty(auth.Parameter))
                throw new UnauthorizedException();
        }
    }
}
