﻿using DesafioCS.Infra.CrossCutting.Security.JWT;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DesafioCS.Infra.CrossCutting.Security.Tests
{
    [TestClass]
    public class JWTUtilTests
    {
        [TestMethod]
        public void Gerar_GeraTokenCorretamente()
        {
            //arrange
            var token = string.Empty;

            //act
            token = JWTUtil.Gerar();

            //assert
            Assert.IsTrue(token.Split('.').Length == 3);
        }

        [TestMethod]
        public void Decodificar_TokenValido_DecodificaCorretamente()
        {
            //arrange
            var token = JWTUtil.Gerar();

            //act
            var decode = JWTUtil.Decodificar(token);

            //assert
            Assert.IsNotNull(decode);
        }
    }
}
