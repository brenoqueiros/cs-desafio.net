﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DesafioCS.Infra.CrossCutting.Security.Hash;

namespace DesafioCS.Infra.CrossCutting.Security.Tests
{
    [TestClass]
    public class CriptoTests
    {
        [TestMethod]
        public void ToSHA256_InputValido_CriptografiaCorreta()
        {
            //arrange
            var input = "testandocriptografia";

            var expectedCripto = "990a358667b24d1130eafb2cbed4fbefa763b5475aede552013356df77bb2195";

            //act
            var result = Cripto.ToSHA256(input);

            //assert
            Assert.AreEqual(expectedCripto, result);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ToSHA256_InputInvalido_DisparaException()
        {
            //arrange
            var input = string.Empty;

            //act
            var result = Cripto.ToSHA256(input);            
        }
    }
}