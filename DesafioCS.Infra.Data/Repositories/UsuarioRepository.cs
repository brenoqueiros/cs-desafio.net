﻿using System.Linq;
using DesafioCS.Domain.Entities;
using DesafioCS.Domain.Interfaces.Repositories;
using System;
using Microsoft.Data.Entity;

namespace DesafioCS.Infra.Data.Repositories
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {
        public new Usuario Obter(Func<Usuario, bool> expr)
        {
            return _context.Usuarios.Include(u => u.Telefones).FirstOrDefault(expr);
        }
    }
}
