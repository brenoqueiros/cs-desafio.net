﻿using DesafioCS.Domain.Interfaces.Repositories;
using DesafioCS.Infra.Data.Context;
using Microsoft.Data.Entity;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace DesafioCS.Infra.Data.Repositories
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected DesafioCSContext _context = new DesafioCSContext();

        public void Inserir(TEntity obj)
        {
            _context.Set<TEntity>().Add(obj);
            _context.SaveChanges();
        }

        public bool Any(Expression<Func<TEntity, bool>> expr)
        {
            return _context.Set<TEntity>().Any(expr);
        }

        public TEntity Obter(Func<TEntity, bool> expr)
        {
            return _context.Set<TEntity>().FirstOrDefault(expr);
        }

        public void Atualizar(TEntity obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
