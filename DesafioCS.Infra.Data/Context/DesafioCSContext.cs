﻿using DesafioCS.Domain.Entities;
using DesafioCS.Infra.Data.EntityConfigs;
using Microsoft.Data.Entity;

namespace DesafioCS.Infra.Data.Context
{
    public class DesafioCSContext : DbContext
    {
        public virtual DbSet<Usuario> Usuarios { get; set; }

        public virtual DbSet<Telefone> Telefones { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseInMemoryDatabase();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            new UsuarioMap(modelBuilder.Entity<Usuario>());

            new TelefoneMap(modelBuilder.Entity<Telefone>());
        }
    }
}
