﻿using DesafioCS.Domain.Entities;
using Microsoft.Data.Entity.Metadata.Builders;

namespace DesafioCS.Infra.Data.EntityConfigs
{
    public class UsuarioMap
    {
        public UsuarioMap(EntityTypeBuilder<Usuario> usuarioBuilder)
        {
            usuarioBuilder.Property(u => u.Nome).HasMaxLength(200);

            usuarioBuilder.Property(u => u.Email).HasMaxLength(100);

            usuarioBuilder.Property(u => u.Senha).HasMaxLength(300);

            usuarioBuilder.Property(u => u.Token).HasMaxLength(300);
        }
    }
}
