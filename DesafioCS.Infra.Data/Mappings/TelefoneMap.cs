﻿using DesafioCS.Domain.Entities;
using Microsoft.Data.Entity.Metadata.Builders;

namespace DesafioCS.Infra.Data.EntityConfigs
{
    public class TelefoneMap
    {
        public TelefoneMap(EntityTypeBuilder<Telefone> telefoneBuilder)
        {
            telefoneBuilder.Property(t => t.DDD).HasMaxLength(5);

            telefoneBuilder.Property(t => t.Numero).HasMaxLength(15);
        }
    }
}
