﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DesafioCS.Application.DTO;
using DesafioCS.Application.AutoMapper;
using System.Collections.Generic;
using System;
using DesafioCS.Infra.CrossCutting.CustomException.Exceptions;
using DesafioCS.Domain.Services;
using DesafioCS.Infra.Data.Repositories;
using DesafioCS.Domain.Interfaces.Repositories;

namespace DesafioCS.Application.Tests
{
    [TestClass]
    public class UsuarioAppServiceTests
    {
        private UsuarioAppService UsuarioAppService { get; set; }

        [TestInitialize]
        public void Inicializar()
        {
            AutoMapperConfig.InitializeMappings();

            var domainService = new UsuarioService(new UsuarioRepository());

            UsuarioAppService = new UsuarioAppService(domainService);
        }

        [TestMethod]
        public void Registrar_UsuarioPreenchido_PreencheCamposCorretamente()
        {
            //arrange
            var novoUsuario = new UsuarioDTO()
            {
                nome = "Usuário Teste",
                email = "teste@teste.com.br",
                senha = "123456",
                telefones = new List<TelefoneDTO>()
                {
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "2222-3333"
                    },
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "99999-9999"
                    }
                }
            };

            //act
            var usuarioRegistrado = UsuarioAppService.Registrar(novoUsuario);

            //assert
            Assert.IsInstanceOfType(usuarioRegistrado.id, typeof(Guid));
            Assert.IsInstanceOfType(usuarioRegistrado.data_criacao, typeof(DateTime));
            Assert.IsInstanceOfType(usuarioRegistrado.data_atualizacao, typeof(DateTime));
            Assert.IsInstanceOfType(usuarioRegistrado.ultimo_login, typeof(DateTime));

            Assert.IsTrue(DateTime.Compare(usuarioRegistrado.data_criacao, usuarioRegistrado.ultimo_login) == -1);
        }

        [TestMethod]
        [ExpectedException(typeof(ExistingUserException))]
        public void Registrar_EmailExistente_ErroEmailExistente()
        {
            //arrange
            var novoUsuario = new UsuarioDTO()
            {
                nome = "Usuário Teste",
                email = "teste@teste.com.br",
                senha = "123456",
                telefones = new List<TelefoneDTO>()
                {
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "2222-3333"
                    },
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "99999-9999"
                    }
                }
            };

            //act
            var usuarioRegistrado = UsuarioAppService.Registrar(novoUsuario);
            UsuarioAppService.Registrar(usuarioRegistrado);

            //assert (throws)
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserException))]
        public void EfetuarLogin_EmailInexistente_UsuarioInvalido()
        {
            //arrange
            var novoUsuario = new UsuarioDTO()
            {
                nome = "Usuário Teste",
                email = "emailinexistente@teste.com.br",
                senha = "123456",
                telefones = new List<TelefoneDTO>()
                {
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "2222-3333"
                    },
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "99999-9999"
                    }
                }
            };

            //act
            UsuarioAppService.EfetuarLogin(novoUsuario);

            //assert (throws)
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserException))]
        public void Registrar_EfetuarLogin_SenhaErrada_SenhaInvalida()
        {
            //arrange
            var novoUsuario = new UsuarioDTO()
            {
                nome = "Usuário Teste",
                email = "senhaerrada@teste.com.br",
                senha = "123456",
                telefones = new List<TelefoneDTO>()
                {
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "2222-3333"
                    },
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "99999-9999"
                    }
                }
            };

            //act
            UsuarioAppService.Registrar(novoUsuario);

            novoUsuario.senha = "654321";

            UsuarioAppService.EfetuarLogin(novoUsuario);

            //assert (throws)
        }

        [TestMethod]
        public void Registrar_EfetuarLogin_UsuarioExistente_AtualizaUltimoLogin()
        {
            //arrange
            var novoUsuario = new UsuarioDTO()
            {
                nome = "Usuário Teste",
                email = "testeatualizalogin@teste.com.br",
                senha = "123456",
                telefones = new List<TelefoneDTO>()
                {
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "2222-3333"
                    },
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "99999-9999"
                    }
                }
            };

            //act
            var usuarioRegistrado = UsuarioAppService.Registrar(novoUsuario);

            usuarioRegistrado.senha = "123456";

            var usuarioAtualizado = UsuarioAppService.EfetuarLogin(usuarioRegistrado);

            //assert
            Assert.IsTrue(usuarioAtualizado.ultimo_login > usuarioRegistrado.ultimo_login);
        }

        [TestMethod]
        [ExpectedException(typeof(UnauthorizedException))]
        public void Registrar_ObterPerfil_SemToken_NaoAutorizado()
        {
            //arrange
            var novoUsuario = new UsuarioDTO()
            {
                nome = "Usuário Teste",
                email = "testetokenvazio@teste.com.br",
                senha = "123456",
                telefones = new List<TelefoneDTO>()
                {
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "2222-3333"
                    },
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "99999-9999"
                    }
                }
            };

            //act
            var usuarioRegistrado = UsuarioAppService.Registrar(novoUsuario);

            UsuarioAppService.ObterPerfil(usuarioRegistrado.id, string.Empty);

            //assert throws
        }

        [TestMethod]
        [ExpectedException(typeof(UnauthorizedException))]
        public void Registrar_ObterPerfil_TokenIncorreto_NaoAutorizado()
        {
            //arrange
            var novoUsuario = new UsuarioDTO()
            {
                nome = "Usuário Teste",
                email = "teste1232344@teste.com.br",
                senha = "123456",
                telefones = new List<TelefoneDTO>()
                {
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "2222-3333"
                    },
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "99999-9999"
                    }
                }
            };

            //act
            var usuarioRegistrado = UsuarioAppService.Registrar(novoUsuario);

            UsuarioAppService.ObterPerfil(usuarioRegistrado.id, "!@#!@#tokenincorretoteste@#$!@#");

            //assert throws
        }

        [TestMethod]
        public void Registrar_ObterPerfil_UltimoLoginMenosDe30Minutos_RetornaPerfil()
        {
            //arrange
            var novoUsuario = new UsuarioDTO()
            {
                nome = "Usuário Teste",
                email = "emailteste@teste.com.br",
                senha = "123456",
                telefones = new List<TelefoneDTO>()
                {
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "2222-3333"
                    },
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "99999-9999"
                    }
                }
            };

            //act
            var usuarioRegistrado = UsuarioAppService.Registrar(novoUsuario);

            var perfil = UsuarioAppService.ObterPerfil(usuarioRegistrado.id, usuarioRegistrado.token);

            //assert
            Assert.IsNotNull(perfil);
        }

        [TestMethod]
        public void Registrar_ObterPerfil_DadosValidos_RetornaPerfil()
        {
            //arrange
            var novoUsuario = new UsuarioDTO()
            {
                nome = "Usuário Teste",
                email = "testeperfilok@teste.com.br",
                senha = "123456",
                telefones = new List<TelefoneDTO>()
                {
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "2222-3333"
                    },
                    new TelefoneDTO()
                    {
                        ddd = "021",
                        numero = "99999-9999"
                    }
                }
            };

            //act
            var usuarioRegistrado = UsuarioAppService.Registrar(novoUsuario);

            var perfil = UsuarioAppService.ObterPerfil(usuarioRegistrado.id, usuarioRegistrado.token);

            //assert
            Assert.IsNotNull(perfil);
        }
    }
}
