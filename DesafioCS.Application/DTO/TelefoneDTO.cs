﻿namespace DesafioCS.Application.DTO
{
    public class TelefoneDTO
    {
        public string ddd { get; set; }

        public string numero { get; set; }
    }
}