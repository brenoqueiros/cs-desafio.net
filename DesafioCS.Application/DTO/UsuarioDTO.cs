﻿using System;
using System.Collections.Generic;

namespace DesafioCS.Application.DTO
{
    public class UsuarioDTO
    {
        public Guid id { get; set; }

        public string nome{ get; set; }

        public string email { get; set; }

        public string senha { get; set; }

        public List<TelefoneDTO> telefones { get; set; }
        
        public DateTime data_criacao { get; set; }

        public DateTime data_atualizacao { get; set; }

        public DateTime ultimo_login { get; set; }

        public string token { get; set; }
    }
}