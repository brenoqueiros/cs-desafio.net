﻿using AutoMapper;
using DesafioCS.Application.DTO;
using DesafioCS.Application.Interfaces;
using DesafioCS.Domain.Entities;
using DesafioCS.Domain.Interfaces.Services;
using DesafioCS.Infra.CrossCutting.CustomException.Exceptions;
using DesafioCS.Infra.CrossCutting.Security.Hash;
using DesafioCS.Infra.CrossCutting.Security.JWT;
using System;

namespace DesafioCS.Application
{
    public class UsuarioAppService : AppServiceBase<Usuario>, IUsuarioAppService
    {
        private readonly IUsuarioService _usuarioService;

        public UsuarioAppService(IUsuarioService usuarioService)
            : base(usuarioService)
        {
            _usuarioService = usuarioService;
        }

        public UsuarioDTO Registrar(UsuarioDTO usuarioDTO)
        {
            if (Any(u => u.Email == usuarioDTO.email))
                throw new ExistingUserException();

            var usuarioDomain = Mapper.Map<Usuario>(usuarioDTO);

            usuarioDomain.Token = JWTUtil.Gerar();

            usuarioDomain.Senha = Cripto.ToSHA256(usuarioDTO.senha);

            Inserir(usuarioDomain);

            return Mapper.Map<UsuarioDTO>(usuarioDomain);
        }

        public UsuarioDTO EfetuarLogin(UsuarioDTO usuarioDTO)
        {
            var usuario = Obter(u => u.Email == usuarioDTO.email);

            if (usuario == null)
                throw new InvalidUserException();

            if (!usuario.ValidarSenha(Cripto.ToSHA256(usuarioDTO.senha)))
                throw new InvalidUserException();

            usuario.AtualizarUltimoLogin();

            Atualizar(usuario);

            return Mapper.Map<UsuarioDTO>(usuario);
        }

        public UsuarioDTO ObterPerfil(Guid id, string token)
        {
            var usuario = _usuarioService.Obter(u => u.Id == id);

            if (!usuario.ValidarToken(token))
                throw new UnauthorizedException();

            var minutosLogado = DateTime.Now.Subtract(usuario.UltimoLogin).Minutes;

            if (!(minutosLogado < 30))
                throw new SessionExpiredException();

            return Mapper.Map<UsuarioDTO>(usuario);
        }
    }
}