﻿using DesafioCS.Application.Interfaces;
using DesafioCS.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace DesafioCS.Application
{
    public class AppServiceBase<TEntity> : IDisposable, IAppServiceBase<TEntity> where TEntity : class
    {
        private readonly IServiceBase<TEntity> _serviceBase;

        public AppServiceBase(IServiceBase<TEntity> serviceBase)
        {
            _serviceBase = serviceBase;
        }

        public void Inserir(TEntity obj)
        {
            _serviceBase.Inserir(obj);
        }

        public bool Any(Expression<Func<TEntity, bool>> expr)
        {
            return _serviceBase.Any(expr);
        }

        public TEntity Obter(Func<TEntity, bool> expr)
        {
            return _serviceBase.Obter(expr);
        }

        public void Atualizar(TEntity obj)
        {
            _serviceBase.Atualizar(obj);
        }

        public void Dispose()
        {
            _serviceBase.Dispose();
        }
    }
}
