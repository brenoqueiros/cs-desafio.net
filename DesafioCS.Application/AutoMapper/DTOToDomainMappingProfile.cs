﻿using AutoMapper;
using DesafioCS.Application.DTO;
using DesafioCS.Domain.Entities;

namespace DesafioCS.Application.AutoMapper
{
    public class DTOToDomainMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<UsuarioDTO, Usuario>();
            CreateMap<TelefoneDTO, Telefone>();

            SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
            DestinationMemberNamingConvention = new PascalCaseNamingConvention();
        }
    }
}