﻿using AutoMapper;

namespace DesafioCS.Application.AutoMapper
{
    public class AutoMapperConfig
    {
        public static void InitializeMappings()
        {
            Mapper.Initialize(i =>
                {
                    i.AddProfile<DomainToDTOMappingProfile>();
                    i.AddProfile<DTOToDomainMappingProfile>();
                }
            );
        }
    }
}
