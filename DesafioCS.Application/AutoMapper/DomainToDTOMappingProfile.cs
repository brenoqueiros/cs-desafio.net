﻿using AutoMapper;
using DesafioCS.Application.DTO;
using DesafioCS.Domain.Entities;

namespace DesafioCS.Application.AutoMapper
{
    public class DomainToDTOMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Usuario, UsuarioDTO>();
            CreateMap<Telefone, TelefoneDTO>();

            SourceMemberNamingConvention = new PascalCaseNamingConvention();
            DestinationMemberNamingConvention = new LowerUnderscoreNamingConvention();
        }
    }
}