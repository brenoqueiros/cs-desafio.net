﻿using DesafioCS.Application.DTO;
using DesafioCS.Domain.Entities;
using System;

namespace DesafioCS.Application.Interfaces
{
    public interface IUsuarioAppService : IAppServiceBase<Usuario>
    {
        UsuarioDTO Registrar(UsuarioDTO usuarioDTO);

        UsuarioDTO EfetuarLogin(UsuarioDTO usuarioDTO);

        UsuarioDTO ObterPerfil(Guid id, string token);
    }
}