﻿using System;
using System.Linq.Expressions;

namespace DesafioCS.Application.Interfaces
{
    public interface IAppServiceBase<TEntity> where TEntity : class
    {
        void Inserir(TEntity obj);

        bool Any(Expression<Func<TEntity, bool>> expr);

        TEntity Obter(Func<TEntity, bool> expr);

        void Atualizar(TEntity obj);

        void Dispose();
    }
}
