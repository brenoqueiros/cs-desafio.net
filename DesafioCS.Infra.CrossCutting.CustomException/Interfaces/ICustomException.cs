﻿using System.Net;

namespace DesafioCS.Infra.CrossCutting.CustomException
{
    public interface ICustomException
    {
        HttpStatusCode Codigo { get; }

        string Mensagem { get; }
    }
}