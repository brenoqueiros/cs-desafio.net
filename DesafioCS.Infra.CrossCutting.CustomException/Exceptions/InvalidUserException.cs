﻿using System;
using System.Net;

namespace DesafioCS.Infra.CrossCutting.CustomException.Exceptions
{
    public class InvalidUserException : Exception, ICustomException
    {
        public HttpStatusCode Codigo
        {
            get
            {
                return HttpStatusCode.Unauthorized;
            }
        }

        public string Mensagem
        {
            get
            {
                return "Usuário e/ou senha inválidos";
            }
        }
    }
}
