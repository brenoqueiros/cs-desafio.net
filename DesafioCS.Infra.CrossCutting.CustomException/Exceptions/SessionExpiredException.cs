﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DesafioCS.Infra.CrossCutting.CustomException.Exceptions
{
    public class SessionExpiredException : Exception, ICustomException
    {
        public HttpStatusCode Codigo
        {
            get
            {
                return HttpStatusCode.Unauthorized;
            }
        }

        public string Mensagem
        {
            get
            {
                return "Sessão inválida";
            }
        }
    }
}
