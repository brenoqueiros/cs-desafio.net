﻿using System;
using System.Net;

namespace DesafioCS.Infra.CrossCutting.CustomException.Exceptions
{
    public class ResourceNotFoundException : Exception, ICustomException
    {
        public HttpStatusCode Codigo
        {
            get
            {
                return HttpStatusCode.NotFound;
            }
        }

        public string Mensagem
        {
            get
            {
                return "URL não encontrada";
            }
        }
    }
}
