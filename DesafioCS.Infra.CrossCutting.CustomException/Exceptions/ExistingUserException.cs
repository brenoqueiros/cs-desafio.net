﻿using System;
using System.Net;

namespace DesafioCS.Infra.CrossCutting.CustomException.Exceptions
{
    public class ExistingUserException : Exception, ICustomException
    {
        public HttpStatusCode Codigo
        {
            get
            {
                return HttpStatusCode.Forbidden;
            }
        }

        public string Mensagem
        {
            get
            {
                return "E-mail já existente";
            }
        }
    }
}
