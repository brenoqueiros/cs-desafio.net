﻿using JWT;
using System;
using System.Collections.Generic;

namespace DesafioCS.Infra.CrossCutting.Security.JWT
{
    public static class JWTUtil
    {
        private const string key = "ca87460dcb43482c0d114b7b7893a8bd5870d01faed17b277c01d35898dd7fcb";

        public static string Gerar()
        {
            var payload = new
            {
                generateDate = DateTime.Now,
                expDate = DateTime.Now.AddMinutes(30)
            };

            return JsonWebToken.Encode(payload, key, JwtHashAlgorithm.HS256);
        }

        public static Dictionary<string, object> Decodificar(string token)
        {
            return JsonWebToken.DecodeToObject<Dictionary<string, object>>(token, key, false);
        }
    }
}