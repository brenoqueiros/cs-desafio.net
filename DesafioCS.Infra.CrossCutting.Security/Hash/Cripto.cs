﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace DesafioCS.Infra.CrossCutting.Security.Hash
{
    public class Cripto
    {
        public static string ToSHA256(string input)
        {
            if (string.IsNullOrEmpty(input))
                throw new Exception("Informe um input válido para criptografar");

            var sb = new StringBuilder();

            using (SHA256 hash = SHA256.Create())
            {
                var enc = Encoding.UTF8;

                byte[] result = hash.ComputeHash(enc.GetBytes(input));

                foreach (byte b in result)
                    sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }
    }
}
